package cm.rostana.camera.Logger;

import android.content.Context;
import android.util.Log;

public class Logger {

    private static final Logger ourInstance = new Logger();

    private Context context;

    private Logger() {}

    public static Logger getInstance() {
        return ourInstance;
    }

    public void Init(Context context) {
        this.context = context;
    }

    public void push(String... args) {
        if (context != null && args.length > 0) {
            StringBuilder buffer = new StringBuilder();
            int index = 0;
            for (String s : args) {
                buffer.append(s);
                if (index < args.length - 1) buffer.append(" -> ");
                ++index;
            }
            Log.e(context.getPackageName(), buffer.toString());
        }
    }

    public void push(byte... args) {
        if (context != null && args.length > 0) {
            StringBuilder buffer = new StringBuilder();
            int index = 0;
            for (byte s : args) {
                buffer.append(s);
                if (index < args.length - 1) buffer.append(" -> ");
                ++index;
            }
            Log.e(context.getPackageName(), buffer.toString());
        }
    }
}
