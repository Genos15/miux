package cm.rostana.camera.Format;

public enum Filter {
    NONE,
    EFFECT_BLACKBOARD,
    EFFECT_NEGATIVE,
    EFFECT_SEPIA
}
