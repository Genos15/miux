package cm.rostana.camera;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import cm.rostana.camera.Format.Filter;
import cm.rostana.camera.Logger.Logger;

public class MainActivity extends AppCompatActivity {

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_CAMERA_PERMISSION = 200;

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    protected CameraDevice mCamera;
    protected CameraCaptureSession mSession;
    protected CaptureRequest.Builder mRequestBuilder;
    private TextureView mTextureView;
    private Size mDimension;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private Animation scale;
    private int ActiveCamera;
    private boolean FlashOn = false, PanelState = false;
    private Filter ActiveFilter;

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            Logger.getInstance().push("onOpened");
            mCamera = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            mCamera.close();
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            mCamera.close();
            mCamera = null;
        }
    };

    private TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            OpenCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Logger.getInstance().Init(this);
        mTextureView = findViewById(R.id.texture);

        assert mTextureView != null;
        mTextureView.setSurfaceTextureListener(textureListener);
        ImageButton main = findViewById(R.id.btn_takepicture),
                change = findViewById(R.id.btn_switch),
                filter = findViewById(R.id.btn_filter),
                flash = findViewById(R.id.btn_flash);

        Button grain = findViewById(R.id.btn_grain),
                sepia = findViewById(R.id.btn_sepia),
                negative = findViewById(R.id.btn_negative),
                none = findViewById(R.id.btn_none);

        assert main != null;

        main.setOnClickListener(v -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale);
            v.startAnimation(scale);
            takePicture();
        });

        ActiveCamera = CameraMetadata.LENS_FACING_BACK;

        flash.setOnClickListener(v -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small);
            v.startAnimation(scale);
            if (FlashOn) flash.setImageResource(R.drawable.ic_flash_off);
            else flash.setImageResource(R.drawable.ic_flash);
            FlashOn = !FlashOn;
        });

        change.setOnClickListener(v -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small);
            v.startAnimation(scale);
            ActiveCamera = ActiveCamera == CameraMetadata.LENS_FACING_BACK ? CameraMetadata.LENS_FACING_FRONT : CameraMetadata.LENS_FACING_BACK;
            mCamera.close();
            OpenCamera();
            if (ActiveCamera == CameraMetadata.LENS_FACING_FRONT) flash.setVisibility(View.GONE);
            else flash.setVisibility(View.VISIBLE);
        });

        filter.setOnClickListener(v -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small);
            v.startAnimation(scale);
            LinearLayout panel = findViewById(R.id.panel);
            int height = (int) (panel.getHeight() * -1.2);

            if (PanelState) {
                change.setClickable(true);
                main.setClickable(true);
                ObjectAnimator mover = ObjectAnimator.ofFloat(panel, "translationY", height, 0);
                mover.start();
            } else {
                change.setClickable(false);
                main.setClickable(false);
                ObjectAnimator mover = ObjectAnimator.ofFloat(panel, "translationY", 0, height);
                mover.start();
            }
            PanelState = !PanelState;
        });

        none.setOnClickListener(view -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small);
            view.startAnimation(scale);
            ActiveFilter = Filter.NONE;
            UpdatePreview();
        });

        grain.setOnClickListener(view -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small);
            view.startAnimation(scale);
            ActiveFilter = Filter.EFFECT_BLACKBOARD;
            UpdatePreview();
        });

        sepia.setOnClickListener(view -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small);
            view.startAnimation(scale);
            ActiveFilter = Filter.EFFECT_SEPIA;
            UpdatePreview();
        });

        negative.setOnClickListener(view -> {
            scale = AnimationUtils.loadAnimation(this, R.anim.anim_scale_small);
            view.startAnimation(scale);
            ActiveFilter = Filter.EFFECT_NEGATIVE;
            UpdatePreview();
        });

        mTextureView.setOnClickListener(v -> {
            LinearLayout panel = findViewById(R.id.panel);
            int height = (int) (panel.getHeight() * -1.2);

            if (PanelState) {
                change.setClickable(true);
                main.setClickable(true);
                ObjectAnimator mover = ObjectAnimator.ofFloat(panel, "translationY", height, 0);
                mover.start();
            }
        });

        ActiveFilter = Filter.NONE;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decoView = getWindow().getDecorView();
        if (hasFocus) {
            decoView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    protected void StartBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    protected void StopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void takePicture() {
        if (null == mCamera) return;
        if (FlashOn) this.TurnFlashOn();
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (manager == null) return;
            CameraCharacteristics details = manager.getCameraCharacteristics(mCamera.getId());
            if (!Objects.nonNull(details.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)))
                return;
            Size[] jpegSizes = Objects.requireNonNull(details.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)).getOutputSizes(ImageFormat.JPEG);

            int width = 640;
            int height = 480;
            if (jpegSizes != null && 0 < jpegSizes.length) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }
            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(mTextureView.getSurfaceTexture()));
            final CaptureRequest.Builder captureBuilder = mCamera.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());


            if (ActiveFilter == Filter.EFFECT_NEGATIVE)
                captureBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_NEGATIVE);
            else if (ActiveFilter == Filter.EFFECT_BLACKBOARD)
                captureBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_BLACKBOARD);
            else if (ActiveFilter == Filter.EFFECT_SEPIA)
                captureBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_SEPIA);
            else
                captureBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_MODE_AUTO);


            int rotation;
            if (ActiveCamera == CameraMetadata.LENS_FACING_FRONT) {
                rotation = Surface.ROTATION_270;
            } else {
                rotation = Surface.ROTATION_90;
            }
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));


            DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_ddHH_mm_ss");
            Date date = new Date();


            String name = "pic" + dateFormat.format(date) + ".jpg";
            final File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + name);
            final ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    try (Image image = reader.acquireLatestImage()) {
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                private void save(byte[] bytes) throws IOException {
                    try (OutputStream output = new FileOutputStream(file)) {
                        output.write(bytes);
                        Toast.makeText(MainActivity.this, "the file has been written", Toast.LENGTH_SHORT).show();
                    }
                }
            };
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                    values.put(MediaStore.MediaColumns.DATA, file.toString());
                    MainActivity.this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    Toast.makeText(MainActivity.this, "Saved In:" + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
                    createCameraPreview();
                }
            };
            mCamera.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } finally {
            if (FlashOn) this.TurnFlashOff();
        }
    }

    protected void createCameraPreview() {
        try {
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mDimension.getWidth(), mDimension.getHeight());
            Surface surface = new Surface(texture);
            mRequestBuilder = mCamera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mRequestBuilder.addTarget(surface);
            mCamera.createCaptureSession(Collections.singletonList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (null == mCamera) return;
                    mSession = cameraCaptureSession;
                    UpdatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(MainActivity.this, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void OpenCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (manager == null) return;
            CameraCharacteristics detail = null;
            String cameraId = null;
            for (String id : manager.getCameraIdList()) {
                detail = manager.getCameraCharacteristics(id);
                if (detail.get(CameraCharacteristics.LENS_FACING) == null) continue;
                if (Objects.nonNull(detail.get(CameraCharacteristics.LENS_FACING)) && Objects.requireNonNull(detail.get(CameraCharacteristics.LENS_FACING)) == ActiveCamera) {
                    cameraId = id;
                    break;
                }
            }
            if (cameraId == null) return;
            StreamConfigurationMap map = detail.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            mDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Logger.getInstance().push("OpenCamera X");
    }

    protected void UpdatePreview() {
        if (null == mCamera) {
            return;
        }
        ApplyFilter(ActiveFilter);
        mRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            mSession.setRepeatingRequest(mRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void CloseCamera() {
        if (null != mCamera) {
            mCamera.close();
            mCamera = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(MainActivity.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override

    protected void onResume() {
        super.onResume();
        Logger.getInstance().push("onResume");
        StartBackgroundThread();
        if (mTextureView.isAvailable()) {
            OpenCamera();
        } else {
            mTextureView.setSurfaceTextureListener(textureListener);
        }
    }

    @Override
    protected void onPause() {
        CloseCamera();
        StopBackgroundThread();
        super.onPause();
    }

    public void TurnFlashOn() {
        if (null == mCamera) {
            return;
        }
        try {
            mRequestBuilder.set(CaptureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_TORCH);
            mSession.setRepeatingRequest(mRequestBuilder.build(), null, mBackgroundHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void TurnFlashOff() {
        new Thread(() -> {
            if (null == mCamera) return;
            try {
                Thread.sleep(1000);
                mRequestBuilder.set(CaptureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_OFF);
                mSession.setRepeatingRequest(mRequestBuilder.build(), null, mBackgroundHandler);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

    }

    public void ApplyFilter(Filter filter) {
        if (null == mCamera) {
            return;
        }
        try {
            mRequestBuilder.set(CaptureRequest.COLOR_CORRECTION_ABERRATION_MODE, CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY);

            switch (filter) {
                case EFFECT_BLACKBOARD: {
                    mRequestBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_AQUA);
                    break;
                }
                case EFFECT_SEPIA: {
                    mRequestBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_SEPIA);
                    break;
                }
                case EFFECT_NEGATIVE: {
                    mRequestBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_NEGATIVE);
                    break;
                }
                default: {
                    mRequestBuilder.set(CaptureRequest.CONTROL_EFFECT_MODE, CameraMetadata.CONTROL_EFFECT_MODE_OFF);
                    break;
                }
            }
            mSession.setRepeatingRequest(mRequestBuilder.build(), null, mBackgroundHandler);
        } catch (Exception e) {
            Logger.getInstance().push(e.getMessage());
        }
    }
}